package net.protolauncher.mojang.version;

import com.google.gson.annotations.SerializedName;

/**
 * Defines the different types that game versions can be.
 */
public enum VersionType {

    @SerializedName("release")
    RELEASE,

    @SerializedName("snapshot")
    SNAPSHOT,

    @SerializedName("old_beta")
    OLD_BETA,

    @SerializedName("old_alpha")
    OLD_ALPHA

}
