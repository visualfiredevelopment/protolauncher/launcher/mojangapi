package net.protolauncher.mojang.version;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import javafx.util.Pair;
import net.protolauncher.util.ISaveable;
import net.protolauncher.util.Network;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Arrays;

/**
 * Represents all Minecraft versions publicly provided by Mojang.
 * Can only be created via GSON parsing.
 */
public class VersionManifest implements ISaveable {

    // Constants
    public static final Path FILE_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON
    private static VersionManifest instance;

    // Static Initializer
    static {
        FILE_LOCATION = Path.of("mojang/version_manifest.json");
        GSON = new GsonBuilder().serializeNulls().create();
    }

    // Instance Variables
    private Latest latest;
    private VersionInfo[] versions;

    // Constructor
    private VersionManifest() { }

    // Getters
    public VersionInfo getLatestRelease() {
        return this.getVersion(latest.release);
    }
    public VersionInfo getLatestSnapshot() {
        return this.getVersion(latest.snapshot);
    }
    public String getLatestReleaseId() { return latest.release; }
    public String getLatestSnapshotId() { return latest.snapshot; }
    public VersionInfo[] getAllVersions() {
        return versions;
    }

    // Methods
    /**
     * Fetches all versions of the corresponding type from the manifest.
     *
     * @param type The {@link VersionType}
     * @return A {@link VersionInfo} array containing the matched types
     */
    public VersionInfo[] getVersionsOfType(VersionType type) {
        return Arrays.stream(this.versions).filter(x -> x.getType() == type).toArray(VersionInfo[]::new);
    }

    /**
     * Fetches a specific version using the corresponding version id from the manifest.
     *
     * @param id The id to look for
     * @return A {@link VersionInfo} if found otherwise null
     */
    public @Nullable VersionInfo getVersion(String id) {
        return Arrays.stream(this.versions).filter(x -> x.getId().equalsIgnoreCase(id)).findFirst().orElse(null);
    }

    /**
     * Loads a {@link VersionManifest}.
     *
     * @param manifestUrl The {@link URL} at which to download the manifest if necessary
     * @param nextUpdate The next {@link Instant} that this manifest needs to be updated
     * @return A pair containing the {@link VersionManifest} as its key and a boolean indicating whether or not it updated
     * @throws IOException Thrown if there was an exception while trying to load.
     */
    public static Pair<VersionManifest, Boolean> load(URL manifestUrl, Instant nextUpdate) throws IOException {
        boolean updated;
        if (!Files.exists(FILE_LOCATION) || (Instant.now().isAfter(nextUpdate))) {
            // Download Manifest
            String string = Network.stringify(Network.fetch(manifestUrl));

            // Parse Manifest
            instance = GSON.fromJson(string, VersionManifest.class);
            instance.save();
            updated = true;
        } else {
            if (instance == null) {
                instance = new VersionManifest();
            }
            instance.load();
            updated = false;
        }
        return new Pair<>(instance, updated);
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return TypeToken.get(VersionManifest.class).getType();
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return FILE_LOCATION;
    }

    /**
     * Represents the IDs for the two latest Minecraft versions (snapshot and release).
     * Can only be created via GSON parsing.
     */
    private static class Latest {

        // Instance Variables
        private String release;
        private String snapshot;

        // Constructor
        private Latest() { }

        // Getters
        public String getRelease() {
            return release;
        }
        public String getSnapshot() {
            return snapshot;
        }

        // Stringify
        @Override
        public String toString() {
            return "Latest{" +
                    "release='" + release + '\'' +
                    ", snapshot='" + snapshot + '\'' +
                    '}';
        }

    }

    // Stringify
    @Override
    public String toString() {
        return "VersionManifest{" +
                "latest=" + latest +
                ", versions=" + Arrays.toString(versions) +
                '}';
    }

}
