package net.protolauncher.mojang.version;

import java.util.Date;

/**
 * Contains information about a version such as where the file is located, the file size, and the date is was created.
 * Can only be created via GSON parsing.
 */
public class VersionInfo {

    // Instance Variables
    private String id;
    private VersionType type;
    private String url;
    private Date time;
    private Date releaseTime;

    // Constructor
    private VersionInfo() { }

    // Getters
    public String getId() {
        return id;
    }
    public VersionType getType() {
        return type;
    }
    public String getUrl() {
        return url;
    }
    public Date getTime() {
        return time;
    }
    public Date getReleaseTime() {
        return releaseTime;
    }

    // Stringify
    @Override
    public String toString() {
        return "VersionInfo{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", url='" + url + '\'' +
                ", time=" + time +
                ", releaseTime=" + releaseTime +
                '}';
    }

}
