package net.protolauncher.mojang.version;

import com.google.gson.annotations.SerializedName;
import net.protolauncher.mojang.Artifact;
import org.jetbrains.annotations.Nullable;

/**
 * Represents the primary downloads for a game version such as the client and server.
 * Can only be created via GSON parsing.
 */
public class VersionDownloads {

    // Instance Variables
    private Artifact client;
    @SerializedName("client_mappings")
    @Nullable private Artifact clientMappings;
    private Artifact server;
    @SerializedName("server_mappings")
    @Nullable private Artifact serverMappings;

    // Constructor
    private VersionDownloads() { }

    // Getters

    public Artifact getClient() {
        return client;
    }
    public @Nullable Artifact getClientMappings() {
        return clientMappings;
    }
    public Artifact getServer() {
        return server;
    }
    public @Nullable Artifact getServerMappings() {
        return serverMappings;
    }

    // Stringify
    @Override
    public String toString() {
        return "VersionDownloads{" +
                "client=" + client +
                ", clientMappings=" + clientMappings +
                ", server=" + server +
                ", serverMappings=" + serverMappings +
                '}';
    }

}
