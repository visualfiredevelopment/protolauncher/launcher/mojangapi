package net.protolauncher.mojang.version;

import com.google.gson.*;
import com.google.gson.annotations.JsonAdapter;
import net.protolauncher.mojang.rule.Action;
import net.protolauncher.mojang.rule.Rule;

import java.lang.reflect.Type;

/**
 * Represents different cli arguments required to launch the game for this version.
 * Can only be created via GSON parsing.
 * <br><br>
 * Since Mojang decided to screw all JSON standards and use a mix of strings and objects for the array,
 * we needed a custom type adapter for this entire class. Thank you Mojang for forcing us to use this hacky solution!
 */
@JsonAdapter(VersionArguments.VersionArgumentsJsonAdapter.class)
public class VersionArguments {

    // Instance Variables
    private String game;
    private String jvm;
    private JsonElement gameOriginal;
    private JsonElement jvmOriginal;

    // Constructor
    private VersionArguments() { }

    // Methods
    public VersionArguments merge(VersionArguments arguments) {
        if (arguments == null) {
            return this;
        }

        this.game += arguments.game;
        if (this.gameOriginal == null && arguments.gameOriginal != null) {
            this.gameOriginal = arguments.gameOriginal;
        } else if (this.gameOriginal != null && arguments.gameOriginal != null) {
            this.gameOriginal.getAsJsonArray().addAll(arguments.gameOriginal.getAsJsonArray());
        }
        this.jvm += arguments.jvm;
        if (this.jvmOriginal == null && arguments.jvmOriginal != null) {
            this.jvmOriginal = arguments.jvmOriginal;
        } else if (this.jvmOriginal != null && arguments.jvmOriginal != null) {
            this.jvmOriginal.getAsJsonArray().addAll(arguments.jvmOriginal.getAsJsonArray());
        }

        return this;
    }

    // Getters
    public String getGame() {
        return game;
    }
    public String getJvm() {
        return jvm;
    }

    /**
     * Handles saving the original JSON for the game and jvm arguments and then passes them through to be re-serialized.
     */
    public static class VersionArgumentsJsonAdapter implements JsonDeserializer<VersionArguments>, JsonSerializer<VersionArguments> {

        @Override
        public VersionArguments deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject obj = json.getAsJsonObject();
            VersionArguments versionArguments = new VersionArguments();
            versionArguments.game = new GameArgumentsDeserializer().deserialize(obj.get("game"), Object.class, context);
            versionArguments.jvm = new JVMArgumentsDeserializer().deserialize(obj.get("jvm"), Object.class, context);
            versionArguments.gameOriginal = obj.get("game");
            versionArguments.jvmOriginal = obj.get("jvm");
            return versionArguments;
        }

        @Override
        public JsonElement serialize(VersionArguments src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject obj = new JsonObject();
            obj.add("game", src.gameOriginal);
            obj.add("jvm", src.jvmOriginal);
            return obj;
        }

    }

    /**
     * Deserializes the 'game' cli arguments array in a {@link VersionArguments} JSON object.
     */
    public static class GameArgumentsDeserializer implements JsonDeserializer<String> {

        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            StringBuilder args = new StringBuilder();

            // Handle null
            if (json == null) {
                return null;
            }

            // Parse the types
            JsonArray arr = json.getAsJsonArray();
            for (JsonElement element : arr) {
                // If the element is a primitive, it's a string and they can be appended to the arguments
                if (element.isJsonPrimitive()) {
                    if (args.length() > 0) {
                        args.append(' ');
                    }
                    args.append(element.getAsString());
                }
                // TODO: Handle options and rules
            }

            // Return the final string
            return args.toString();
        }

    }

    /**
     * Deserializes the 'jvm' cli arguments array in a {@link VersionArguments} JSON object.
     */
    public static class JVMArgumentsDeserializer implements JsonDeserializer<String> {

        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            StringBuilder args = new StringBuilder();

            // Handle null
            if (json == null) {
                return null;
            }

            // Parse the types
            JsonArray arr = json.getAsJsonArray();
            for (JsonElement element : arr) {
                // If the element is a primitive, it's a string and they can be appended to the arguments
                if (element.isJsonPrimitive()) {
                    if (args.length() > 0) {
                        args.append(' ');
                    }
                    args.append(element.getAsString());

                // If the element is not a primitive, it's an object (value with rules) and must be parsed further
                } else {
                    JsonObject obj = element.getAsJsonObject();

                    // Fetch the rules from the object
                    Rule[] rules = (new Gson()).fromJson(obj.get("rules"), Rule[].class);

                    // Check the rules to determine if we should even bother parsing the value
                    if (Rule.determineAction(rules) == Action.ALLOW) {

                        // The final value to append to the arguments
                        StringBuilder value = new StringBuilder();

                        // Check if the rule's value contains an array or a string
                        JsonElement valueElement = obj.get("value");

                        // If the element is a primitive, it's a string and they can be appended to the arguments
                        if (valueElement.isJsonPrimitive()) {
                            value = new StringBuilder(valueElement.getAsString());

                            // Otherwise, the element is an array, it's an array of string values and we should convert it to be one string
                        } else {
                            JsonArray multivalueElement = valueElement.getAsJsonArray();
                            for (JsonElement multivalueValue : multivalueElement) {
                                if (value.length() > 0) {
                                    value.append(' ');
                                }
                                value.append('"').append(multivalueValue.getAsString()).append('"');
                            }
                        }

                        // Append the value
                        if (args.length() > 0) {
                            args.append(' ');
                        }
                        args.append(value);
                    }
                }
            }

            // Return the final string
            return args.toString();
        }

    }

    // Stringify
    @Override
    public String toString() {
        return "VersionArguments{" +
                "game='" + game + '\'' +
                ", jvm='" + jvm + '\'' +
                '}';
    }

}
