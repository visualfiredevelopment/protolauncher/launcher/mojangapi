package net.protolauncher.mojang.version;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.protolauncher.mojang.Artifact;
import net.protolauncher.mojang.library.Library;
import net.protolauncher.util.ISaveable;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Represents a Minecraft game version as fetched from the {@link VersionManifest}.
 * Can only be created via GSON parsing.
 */
public class Version implements ISaveable {

    // Constants
    public static final Path FOLDER_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON

    // Static Initializer
    static {
        FOLDER_LOCATION = Path.of("mojang/versions/");
        GSON = new GsonBuilder().serializeNulls().create();
    }

    // Instance Variables
    private VersionArguments arguments;
    private Artifact assetIndex; // Not the same as the AssetIndex object, it's an artifact linking to it
    private String assets; // Actually the assets game version but whatever
    private VersionDownloads downloads;
    private String id;
    private Library[] libraries;
    private Logging logging;
    private String mainClass;
    private String minecraftArguments; // Optional; Legacy Versions
    private String minimumLauncherVersion;
    private Date releaseTime;
    private Date time;
    private VersionType type;

    // Constructor
    protected Version() { }

    // Getters
    public VersionArguments getArguments() {
        return arguments;
    }
    public Artifact getAssetIndex() {
        return assetIndex;
    }
    public String getAssets() {
        return assets;
    }
    public VersionDownloads getDownloads() {
        return downloads;
    }
    public String getId() {
        return id;
    }
    public Library[] getLibraries() {
        return libraries;
    }
    public Logging getLogging() {
        return logging;
    }
    public String getMainClass() {
        return mainClass;
    }
    public String getMinecraftArguments() {
        return minecraftArguments;
    }
    public String getMinimumLauncherVersion() {
        return minimumLauncherVersion;
    }
    public Date getReleaseTime() {
        return releaseTime;
    }
    public Date getTime() {
        return time;
    }
    public VersionType getVersionType() {
        return type;
    }

    // Setters
    public Version setId(String id) {
        this.id = id;
        return this;
    }


    // Methods
    /**
     * Returns the FOLDER_LOCATION resolved to id/
     *
     * @param id The ID to use to get the path
     * @return The full path of the version's folder
     */
    public static Path getFolderPath(String id) {
        return FOLDER_LOCATION.resolve(id + "/");
    }

    /**
     * Returns the FOLDER_LOCATION resolved to the id/id.json
     *
     * @param id The ID to use to get the path
     * @return The full path of the JSON file
     */
    public static Path getFilePath(String id) {
        return getFolderPath(id).resolve(id + ".json");
    }

    /**
     * Merges this version with another version. Useful for Minecraft Forge parsing.
     * Prioritizes this version. Things like IDs, times, and more will not transfer.
     * Main-Class will transfer from the merged version.
     * Arguments are appended.
     * Libraries are combined.
     *
     * @param version The version to merge into this one.
     * @return The merged {@link Version}
     */
    public Version merge(Version version, boolean ourLibsFirst) {
        this.mainClass = version.mainClass;
        this.arguments = this.arguments.merge(version.arguments);
        List<Library> list;
        if (ourLibsFirst) {
            list = new ArrayList<>(Arrays.asList(this.libraries));
            list.addAll(Arrays.asList(version.libraries));
        } else {
            list = new ArrayList<>(Arrays.asList(version.libraries));
            list.addAll(Arrays.asList(this.libraries));
        }
        this.libraries = list.toArray(Library[]::new);
        if (this.minecraftArguments == null) {
            this.minecraftArguments = version.minecraftArguments;
        } else {
            this.minecraftArguments += version.minecraftArguments;
        }
        return this;
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return TypeToken.get(Version.class).getType();
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return getFilePath(id);
    }

    /**
     * Represents the logging for the client (I'm still not certain on this one).
     * Can only be created via GSON parsing.
     */
    public static class Logging {

        // Instance Variables
        private Client client;

        // Constructor
        private Logging() { }

        // Getters
        public Client getClient() {
            return client;
        }

        /**
         * The client as referenced in the {@link Logging} comment.
         * Can only be created via GSON parsing.
         */
        public static class Client {

            // Instance Variables
            private String argument;
            private Artifact file;
            private String type;

            // Constructor
            private Client() { }

            // Getters
            public String getArgument() {
                return argument;
            }
            public Artifact getFile() {
                return file;
            }
            public String getType() {
                return type;
            }

            // Stringify
            @Override
            public String toString() {
                return "Client{" +
                        "argument='" + argument + '\'' +
                        ", file=" + file +
                        ", type='" + type + '\'' +
                        '}';
            }

        }

        // Stringify
        @Override
        public String toString() {
            return "Logging{" +
                    "client=" + client +
                    '}';
        }

    }

    // Stringify
    @Override
    public String toString() {
        return "Version{" +
                "arguments=" + arguments +
                ", assetIndex=" + assetIndex +
                ", assets='" + assets + '\'' +
                ", downloads=" + downloads +
                ", id='" + id + '\'' +
                ", libraries=" + Arrays.toString(libraries) +
                ", logging=" + logging +
                ", mainClass='" + mainClass + '\'' +
                ", minecraftArguments='" + minecraftArguments + '\'' +
                ", minimumLauncherVersion='" + minimumLauncherVersion + '\'' +
                ", releaseTime=" + releaseTime +
                ", time=" + time +
                ", type=" + type +
                '}';
    }

}
