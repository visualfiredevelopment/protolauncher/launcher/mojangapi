package net.protolauncher.mojang;

import org.jetbrains.annotations.Nullable;

/**
 * An artifact, usually stemming from a JSON, represents any piece of data that can be validated via a sha1.
 */
public class Artifact {

    // Instance Variables
    @Nullable private String id;
    @Nullable private String path;
    private String sha1;
    private long size;
    private long totalSize;
    @Nullable private String url;

    // Constructor
    public Artifact(@Nullable String id, @Nullable String path, String sha1, long size, long totalSize, @Nullable String url) {
        this.id = id;
        this.path = path;
        this.sha1 = sha1;
        this.size = size;
        this.totalSize = totalSize;
        this.url = url;
    }

    // Getters
    public @Nullable String getId() {
        return id;
    }
    public @Nullable String getPath() {
        return path;
    }
    public String getSha1() {
        return sha1;
    }
    public long getSize() {
        return size;
    }
    public long getTotalSize() {
        return totalSize;
    }
    public @Nullable String getUrl() {
        return url;
    }

    // Stringify
    @Override
    public String toString() {
        return "Artifact{" +
                "id='" + id + '\'' +
                ", path='" + path + '\'' +
                ", sha1='" + sha1 + '\'' +
                ", size=" + size +
                ", totalSize=" + totalSize +
                ", url='" + url + '\'' +
                '}';
    }

}
