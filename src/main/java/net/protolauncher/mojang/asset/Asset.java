package net.protolauncher.mojang.asset;

/**
 * Represents an asset from an {@link AssetIndex}.
 * Very similar to a {@link net.protolauncher.mojang.Artifact} but with slightly different variable names.
 * Can only be created via GSON parsing.
 */
public class Asset {

    // Instance Variables
    private String hash;
    private long size;

    // Constructor
    private Asset() { }

    // Getters
    public String getHash() {
        return hash;
    }
    public String getId() {
        return hash.substring(0, 2);
    }
    public long getSize() {
        return size;
    }

    // Stringify
    @Override
    public String toString() {
        return "Asset{" +
                "hash='" + hash + '\'' +
                ", size=" + size +
                '}';
    }

}
