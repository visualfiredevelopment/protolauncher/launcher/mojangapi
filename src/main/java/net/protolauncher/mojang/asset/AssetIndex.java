package net.protolauncher.mojang.asset;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import net.protolauncher.util.ISaveable;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.HashMap;

/**
 * Represents an index for all of the assets for a game version.
 */
public class AssetIndex implements ISaveable {

    // Constants
    public static final Path FOLDER_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON

    // Static Initializer
    static {
        FOLDER_LOCATION = Path.of("mojang/assets/");
        GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    }

    // Instance Variables
    private HashMap<String, Asset> objects;
    private boolean virtual;
    @SerializedName("map_to_resources")
    private boolean mapToResources;
    private String id;

    // Constructor
    public AssetIndex(String id) {
        this.id = id;
    }

    // Getters
    public HashMap<String, Asset> getObjects() {
        return objects;
    }
    public boolean isVirtual() {
        return virtual;
    }
    public boolean isMapToResources() {
        return mapToResources;
    }
    public String getId() {
        return id;
    }

    // Setters
    public AssetIndex setId(String id) {
        this.id = id;
        return this;
    }

    // Methods
    /**
     * Returns the FOLDER_LOCATION resolved to indexes/{id}.json
     *
     * @param id The ID to use to get the path
     * @return The full path of the JSON file
     */
    public static Path getFilePath(String id) {
        return FOLDER_LOCATION.resolve("indexes/" + id + ".json");
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return TypeToken.get(AssetIndex.class).getType();
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        return getFilePath(id);
    }

    // Stringify
    @Override
    public String toString() {
        return "AssetIndex{" +
                "objects=" + objects +
                ", virtual=" + virtual +
                ", mapToResources=" + mapToResources +
                ", id='" + id + '\'' +
                '}';
    }

}
