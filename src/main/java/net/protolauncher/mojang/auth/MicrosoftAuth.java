package net.protolauncher.mojang.auth;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.protolauncher.util.Network;
import org.jetbrains.annotations.Nullable;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public class MicrosoftAuth {

    // Variables
    private Gson gson; // A gson parser to use for all requests
    private String clientId; // The Azure Client ID to use for authentication
    private URL redirectUrl; // The return redirect URL (must be registered on the Azure Client ID)
    private String oauthUrl; // The Microsoft OAuth2 URL
    private String oauthTokenUrl; // The Microsoft OAuth2 Token URL
    private String xblUrl; // The Xbox Live OAuth2 URL
    private String xstsUrl; // The "XSTS" URL
    private String mcsUrl; // The Minecraft Services OAuth2 URL

    // Constructor
    public MicrosoftAuth(Gson gson, String clientId, URL redirectUrl, String oauthUrl, String oauthTokenUrl, String xblUrl, String xstsUrl, String mcsUrl) {
        this.gson = gson;
        this.clientId = clientId;
        this.redirectUrl = redirectUrl;
        this.oauthUrl = oauthUrl;
        this.oauthTokenUrl = oauthTokenUrl;
        this.xblUrl = xblUrl;
        this.xstsUrl = xstsUrl;
        this.mcsUrl = mcsUrl;
    }

    /**
     * Creates a new JavaFX window and prompts the user to sign in with their Microsoft account.
     *
     * @param owner The owner of the popup window.
     * @return A {@link CompletableFuture} that will timeout in 5 minutes containing either null if it errored with no exception or a string with the code.
     */
    public CompletableFuture<String> promptMicrosoftLogin(@Nullable Stage owner) {
        CompletableFuture<String> future = new CompletableFuture<>();

        Platform.runLater(() -> {
            // Create popup window
            Stage stage = new Stage();
            stage.initOwner(owner);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setMinWidth(500);
            stage.setMinHeight(600);
            stage.setWidth(500);
            stage.setHeight(600);
            stage.setTitle("ProtoLauncher: Microsoft Login");
            if (owner != null) {
                stage.setX(owner.getX() + (owner.getWidth() / 2) - (stage.getWidth() / 2));
                stage.setY(owner.getY() + (owner.getHeight() / 2) - (stage.getHeight() / 2));
                stage.getIcons().add(owner.getIcons().get(0));
            }

            // Handle Stage Closure
            AtomicBoolean validClose = new AtomicBoolean(false);
            stage.setOnHiding(event -> {
                if (!validClose.get()) {
                    future.complete(null);
                }
            });

            // Create WebView w/ Cookies
            CookieManager manager = new CookieManager();
            CookieHandler.setDefault(manager);
            WebView view = new WebView();
            WebEngine engine = view.getEngine();
            engine.locationProperty().addListener(event -> {
                String location = view.getEngine().getLocation();

                try {
                    URL url = new URL(location);
                    if (location.contains("nativeclient") && url.getHost().equals(redirectUrl.getHost())) {
                        validClose.set(true);
                        stage.close();

                        if (url.getQuery().contains("error=")) {
                            if (url.getQuery().contains("access_denied")) {
                                future.complete(null);
                            } else {
                                future.completeExceptionally(new Exception("There was an error while authenticating! Please contact the developer."));
                            }
                        } else if (url.getQuery().contains("code=")) {
                            String code = location.substring(location.indexOf("?code=") + "?code=".length());
                            future.complete(code);
                        } else {
                            throw new Exception("Unknown response type! Please contact the developer.");
                        }
                    }
                } catch (Exception e) {
                    future.completeExceptionally(e);
                }
            });

            // Create and apply scene
            Scene scene = new Scene(view, 900, 900);
            stage.setScene(scene);
            stage.show();

            // Load the URL
            engine.load(
                    oauthUrl +
                            "?client_id=" + clientId +
                            "&response_type=code" +
                            "&redirect_uri=" + redirectUrl.toString() +
                            "&scope=XboxLive.signin%20offline_access"
            );

            // Handle Timeout
            future.exceptionally(e -> {
                if (e instanceof TimeoutException) {
                    validClose.set(true);
                    stage.close();
                }
                return null;
            });
            future.orTimeout(5, TimeUnit.MINUTES);
        });

        // Return the future
        return future;
    }

    /**
     * Uses the auth code normally acquired by {@link MicrosoftAuth#promptMicrosoftLogin(Stage)} to get an accessToken and a refreshToken.
     *
     * @param authCode The code as acquired by Microsoft login.
     * @return A {@link MicrosoftAuth.MicrosoftResponse}
     * @throws IOException Thrown if there is a network issue
     */
    public MicrosoftResponse authenticateMicrosoft(String authCode) throws IOException {
        // Create Encoded Parameters
        String encoded = "client_id=" + clientId +
                         "&code=" + authCode +
                         "&grant_type=authorization_code" +
                         "&redirect_uri=" + redirectUrl;

        // Create connection
        HttpsURLConnection connection = Network.createConnection(new URL(oauthTokenUrl), "POST", false);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Accept", "application/json");
        try (OutputStream outstream = connection.getOutputStream()) {
            byte[] content = encoded.getBytes();
            outstream.write(content, 0, content.length);
        }

        // Fetch response
        String response = Network.stringify(Network.send(connection, false));

        try {
            return gson.fromJson(response, MicrosoftResponse.class);
        } catch (JsonParseException e) {
            throw new IOException("Internal error while authenticating! Contact developer. Response Content:\n\n" + response);
        }
    }

    /**
     * Uses a refreshToken normally acquired by {@link MicrosoftAuth#authenticateMicrosoft(String)} to refresh an accessToken
     *
     * @param refreshToken The refreshToken as acquired by Microsoft authentication.
     * @return A {@link MicrosoftAuth.MicrosoftResponse}
     * @throws IOException Thrown if there is a network issue
     */
    public MicrosoftResponse refreshMicrosoft(String refreshToken) throws IOException {
        // Create Encoded Parameters
        String encoded = "client_id=" + clientId +
                "&refresh_token=" + refreshToken +
                "&grant_type=refresh_token" +
                "&redirect_uri=" + redirectUrl;

        // Create connection
        HttpsURLConnection connection = Network.createConnection(new URL(oauthTokenUrl), "POST", false);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Accept", "application/json");
        try (OutputStream outstream = connection.getOutputStream()) {
            byte[] content = encoded.getBytes();
            outstream.write(content, 0, content.length);
        }

        // Fetch response
        String response = Network.stringify(Network.send(connection));

        try {
            return gson.fromJson(response, MicrosoftResponse.class);
        } catch (JsonParseException e) {
            throw new IOException("Internal error while authenticating! Contact developer. Response Content:\n\n" + response);
        }
    }

    /**
     * Authenticates with Xbox Live using a Microsoft access token.
     *
     * @param accessToken The Microsoft access token to use
     * @return A {@link MicrosoftAuth.XboxLiveResponse}
     * @throws IOException Thrown if there is a network error - this will throw a 401 error if the access token needs to be refreshed
     */
    public XboxLiveResponse authenticateXboxLive(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(xblUrl));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();
            JsonObject properties = new JsonObject();
            properties.addProperty("AuthMethod", "RPS");
            properties.addProperty("SiteName", "user.auth.xboxlive.com");
            properties.addProperty("RpsTicket", "d=" + accessToken);
            data.add("Properties", properties);
            data.addProperty("RelyingParty", "http://auth.xboxlive.com");
            data.addProperty("TokenType", "JWT");

            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }

        String response = Network.stringify(Network.send(connection));
        try {
            return gson.fromJson(response, XboxLiveResponse.class);
        } catch (JsonParseException e) {
            throw new IOException("Internal error while authenticating! Contact developer. Response Content:\n\n" + response);
        }
    }

    /**
     * Authenticates with XSTS using an Xbox Live token
     *
     * @param xboxLiveToken The Xbox Live token
     * @return A {@link MicrosoftAuth.XboxLiveResponse} - be careful to check for the "XErr" on a 401 response code. See <a href="https://wiki.vg/Microsoft_Authentication_Scheme#Authenticate_with_XSTS">https://wiki.vg/Microsoft_Authentication_Scheme</a>
     * @throws IOException Thrown if there is a network error
     */
    public XboxLiveResponse authenticateXsts(String xboxLiveToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(xstsUrl));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();
            JsonObject properties = new JsonObject();
            properties.addProperty("SandboxId", "RETAIL");
            JsonArray userTokens = new JsonArray();
            userTokens.add(xboxLiveToken);
            properties.add("UserTokens", userTokens);
            data.add("Properties", properties);
            data.addProperty("RelyingParty", "rp://api.minecraftservices.com/");
            data.addProperty("TokenType", "JWT");

            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }

        String response = Network.stringify(Network.send(connection, false));
        try {
            return gson.fromJson(response, XboxLiveResponse.class);
        } catch (JsonParseException e) {
            throw new IOException("Internal error while authenticating! Contact developer. Response Content:\n\n" + response);
        }
    }

    /**
     * Aquires a good ol' Minecraft accessToken using the xsts token and the 'uhs' response option
     *
     * @param xstsToken The XSTS Token from {@link MicrosoftAuth#authenticateXsts(String)}
     * @param uhs The UHS from {@link MicrosoftAuth#authenticateXboxLive(String)} or {@link MicrosoftAuth#authenticateXsts(String)}
     * @return A {@link MicrosoftAuth.MinecraftResponse}
     * @throws IOException Thrown if there is a network error
     */
    public MinecraftResponse authenticateMinecraft(String xstsToken, String uhs) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(mcsUrl + "authentication/login_with_xbox"));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();
            data.addProperty("identityToken", "XBL3.0 x=" + uhs + ";" + xstsToken);

            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }

        String response = Network.stringify(Network.send(connection, false));
        try {
            return gson.fromJson(response, MinecraftResponse.class);
        } catch (JsonParseException e) {
            throw new IOException("Internal error while authenticating! Contact developer. Response Content:\n\n" + response);
        }
    }

    /**
     * Verifies that the account owns Minecraft: Java Edition.
     *
     * @param accessToken The Mojang accessToken aquired by {@link MicrosoftAuth#authenticateMinecraft(String, String)}
     * @return True if the accounts owns it, false if not
     * @throws IOException Thrown if there is a network issue
     */
    public boolean verifyOwnership(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(mcsUrl + "entitlements/mcstore"), "GET");
        connection.setRequestProperty("Authorization", "Bearer " + accessToken);

        String response = Network.stringify(Network.send(connection, false));
        JsonObject obj = gson.fromJson(response, JsonObject.class);
        if (obj.get("items") != null) {
            JsonArray arr = obj.getAsJsonArray("items");
            boolean product = false;
            boolean game = false;
            for (JsonElement element : arr) {
                if (element instanceof JsonObject) {
                    JsonObject item = (JsonObject) element;
                    if (item.get("name") != null && item.get("name").getAsString().equals("product_minecraft")) {
                        product = true;
                    } else if (item.get("name") != null && item.get("name").getAsString().equals("game_minecraft")) {
                        game = true;
                    }
                }
            }
            return product && game;
        } else {
            return false;
        }
    }

    public Profile getProfile(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(mcsUrl + "minecraft/profile"), "GET");
        connection.setRequestProperty("Authorization", "Bearer " + accessToken);

        String response = Network.stringify(Network.send(connection, false));
        return gson.fromJson(response, Profile.class);
    }

    public static class MicrosoftResponse extends Error {

        @SerializedName("token_type")
        private String tokenType;
        @SerializedName("expires_in")
        private String expiresIn;
        private String scope;
        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("refresh_token")
        private String refreshToken;
        @SerializedName("user_id")
        private String userId;
        private String foci;

        public String getTokenType() {
            return tokenType;
        }
        public String getExpiresIn() {
            return expiresIn;
        }
        public String getScope() {
            return scope;
        }
        public String getAccessToken() {
            return accessToken;
        }
        public String getRefreshToken() {
            return refreshToken;
        }
        public String getUserId() {
            return userId;
        }
        public String getFoci() {
            return foci;
        }

        @Override
        public String toString() {
            return "MicrosoftResponse{" +
                    "tokenType='" + tokenType + '\'' +
                    ", expiresIn='" + expiresIn + '\'' +
                    ", scope='" + scope + '\'' +
                    ", accessToken='" + accessToken + '\'' +
                    ", refreshToken='" + refreshToken + '\'' +
                    ", userId='" + userId + '\'' +
                    ", foci='" + foci + '\'' +
                    ", error='" + error + '\'' +
                    ", errorDescription='" + errorDescription + '\'' +
                    ", correlationId='" + correlationId + '\'' +
                    '}';
        }

    }

    public static class XboxLiveResponse extends Error {

        @SerializedName("IssueInstant")
        private String issueInstant;
        @SerializedName("NotAfter")
        private String notAfter;
        @SerializedName("Token")
        private String token;
        @SerializedName("DisplayClaims")
        private DisplayClaims displayClaims;
        @SerializedName("Identity")
        private String identity;
        @SerializedName("XErr")
        private String xerr;
        @SerializedName("Message")
        private String message;
        @SerializedName("Redirect")
        private String redirect;

        public String getIssueInstant() {
            return issueInstant;
        }
        public String getNotAfter() {
            return notAfter;
        }
        public String getToken() {
            return token;
        }
        public DisplayClaims getDisplayClaims() {
            return displayClaims;
        }
        public String getIdentity() {
            return identity;
        }
        public String getXerr() {
            return xerr;
        }
        public String getMessage() {
            return message;
        }
        public String getRedirect() {
            return redirect;
        }

        public static class DisplayClaims {

            private Uhs[] xui;

            public Uhs[] getXui() {
                return xui;
            }

            public static class Uhs {

                private String uhs;

                public String getUhs() {
                    return uhs;
                }

                @Override
                public String toString() {
                    return "Uhs{" +
                            "uhs='" + uhs + '\'' +
                            '}';
                }

            }

            @Override
            public String toString() {
                return "DisplayClaims{" +
                        "xui=" + Arrays.toString(xui) +
                        '}';
            }

        }

        @Override
        public String toString() {
            return "XboxLiveResponse{" +
                    "issueInstant='" + issueInstant + '\'' +
                    ", notAfter='" + notAfter + '\'' +
                    ", token='" + token + '\'' +
                    ", displayClaims=" + displayClaims +
                    ", identity='" + identity + '\'' +
                    ", xerr='" + xerr + '\'' +
                    ", message='" + message + '\'' +
                    ", redirect='" + redirect + '\'' +
                    ", error='" + error + '\'' +
                    ", errorDescription='" + errorDescription + '\'' +
                    ", correlationId='" + correlationId + '\'' +
                    '}';
        }

    }

    public static class MinecraftResponse extends Error {

        private String username;
        private JsonElement[] roles;
        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("token_type")
        private String tokenType;
        @SerializedName("expires_in")
        private String expiresIn;

        public String getUsername() {
            return username;
        }
        public JsonElement[] getRoles() {
            return roles;
        }
        public String getAccessToken() {
            return accessToken;
        }
        public String getTokenType() {
            return tokenType;
        }
        public String getExpiresIn() {
            return expiresIn;
        }

        @Override
        public String toString() {
            return "MinecraftResponse{" +
                    "username='" + username + '\'' +
                    ", roles=" + Arrays.toString(roles) +
                    ", accessToken='" + accessToken + '\'' +
                    ", tokenType='" + tokenType + '\'' +
                    ", expiresIn='" + expiresIn + '\'' +
                    ", error='" + error + '\'' +
                    ", errorDescription='" + errorDescription + '\'' +
                    ", correlationId='" + correlationId + '\'' +
                    '}';
        }

    }

    public static class Profile {

        private String id;
        private String name;
        private Skin[] skins;
        private JsonElement[] capes;

        private String path;
        private String errorType;
        private String error;
        private String errorMessage;
        private String developerMessage;

        public String getId() {
            return id;
        }
        public String getName() {
            return name;
        }
        public Skin[] getSkins() {
            return skins;
        }
        public JsonElement[] getCapes() {
            return capes;
        }
        public String getPath() {
            return path;
        }
        public String getErrorType() {
            return errorType;
        }
        public String getError() {
            return error;
        }
        public String getErrorMessage() {
            return errorMessage;
        }
        public String getDeveloperMessage() {
            return developerMessage;
        }

        public static class Skin {

            private String id;
            private String state;
            private String url;
            private String variant;
            private String alias;

            public String getId() {
                return id;
            }
            public String getState() {
                return state;
            }
            public String getUrl() {
                return url;
            }
            public String getVariant() {
                return variant;
            }
            public String getAlias() {
                return alias;
            }

            @Override
            public String toString() {
                return "Skin{" +
                        "id='" + id + '\'' +
                        ", state='" + state + '\'' +
                        ", url='" + url + '\'' +
                        ", variant='" + variant + '\'' +
                        ", alias='" + alias + '\'' +
                        '}';
            }

        }

        @Override
        public String toString() {
            return "Profile{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", skins=" + Arrays.toString(skins) +
                    ", capes=" + Arrays.toString(capes) +
                    ", path='" + path + '\'' +
                    ", errorType='" + errorType + '\'' +
                    ", error='" + error + '\'' +
                    ", errorMessage='" + errorMessage + '\'' +
                    ", developerMessage='" + developerMessage + '\'' +
                    '}';
        }

    }

    public static class Error {

        protected String error;
        @SerializedName("error_description")
        protected String errorDescription;
        @SerializedName("correlation_id")
        protected String correlationId;

        public String getError() {
            return error;
        }
        public String getErrorDescription() {
            return errorDescription;
        }
        public String getCorrelationId() {
            return correlationId;
        }

        @Override
        public String toString() {
            return "Error{" +
                    "error='" + error + '\'' +
                    ", errorDescription='" + errorDescription + '\'' +
                    ", correlationId='" + correlationId + '\'' +
                    '}';
        }

    }

}
