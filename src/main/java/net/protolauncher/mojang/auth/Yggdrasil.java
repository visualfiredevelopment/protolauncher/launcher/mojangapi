package net.protolauncher.mojang.auth;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.protolauncher.util.Network;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

public class Yggdrasil {

    // Variables
    private Gson gson; // A gson parser to use for all requests
    private String api; // The API endpoint to use for all requests INCLUDING the ending slash
    private String clientToken; // The clientToken to be used for all requests

    // Constructor
    public Yggdrasil(Gson gson, String api, String clientToken) {
        this.gson = gson;
        this.api = api;
        this.clientToken = clientToken;
    }

    /**
     * Sends an authentication JSON request to the authentication API and returns the response
     *
     * @param username The username to authenticate with
     * @param password The password to authenticate with
     * @return A {@link Yggdrasil.Response}
     * @throws IOException Thrown if there is a network issue
     */
    public Response authenticate(String username, String password) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(api + "authenticate"));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();

            // Add Agent
            JsonObject agent = new JsonObject();
            agent.addProperty("name", "Minecraft");
            agent.addProperty("version", 1);
            data.add("agent", agent);

            // Add Username, Password, and ClientToken
            data.addProperty("username", username);
            data.addProperty("password", password);
            data.addProperty("clientToken", clientToken);

            // Request User
            data.addProperty("requestUser", true);

            // Write JSON
            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }
        return gson.fromJson(Network.stringify(Network.send(connection, false)), Response.class);
    }

    /**
     * Returns a boolean indicating whether or not the provided access token is valid
     *
     * @param accessToken The access token to check if valid
     * @return Whether or not the provided access token is valid
     * @throws IOException Thrown if there is a network issue
     */
    public boolean validate(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(api + "validate"));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();

            // Add AccessToken & ClientToken
            data.addProperty("accessToken", accessToken);
            data.addProperty("clientToken", clientToken);

            // Write JSON
            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }
        return connection.getResponseCode() == 204;
    }

    /**
     * Attempts to invalidate the provided access token and returns whether or not it failed
     *
     * @param accessToken The access token to invalidate
     * @return Whether or not the invalidation succeeded
     * @throws IOException Thrown if there is a network issue
     */
    public boolean invalidate(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(api + "invalidate"));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();

            // Add AccessToken & ClientToken
            data.addProperty("accessToken", accessToken);
            data.addProperty("clientToken", clientToken);

            // Write JSON
            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }

        // Get response
        String response = Network.stringify(Network.send(connection));
        return response.isEmpty();
    }

    /**
     * Attempts to refresh the provided access token
     *
     * @param accessToken The token that should be refreshed
     * @return A {@link Yggdrasil.Response}
     * @throws IOException Thrown if there is a network issue
     */
    public Response refresh(String accessToken) throws IOException {
        HttpsURLConnection connection = Network.createConnection(new URL(api + "refresh"));
        try (OutputStream outstream = connection.getOutputStream()) {
            JsonObject data = new JsonObject();

            // Add AccessToken & ClientToken
            data.addProperty("accessToken", accessToken);
            data.addProperty("clientToken", clientToken);

            // Write JSON
            byte[] content = data.toString().getBytes();
            outstream.write(content, 0, content.length);
        }
        return gson.fromJson(Network.stringify(Network.send(connection, false)), Response.class);
    }

    public static class Response extends Error {

        private String accessToken;
        private String clientToken;
        private SelectedProfile selectedProfile;
        private User user;

        public String getAccessToken() {
            return accessToken;
        }
        public String getClientToken() {
            return clientToken;
        }
        public SelectedProfile getSelectedProfile() {
            return selectedProfile;
        }
        public User getUser() {
            return user;
        }

        @Override
        public String toString() {
            return "Yggdrasil.Response{" +
                    "accessToken='" + accessToken + '\'' +
                    ", clientToken='" + clientToken + '\'' +
                    ", selectedProfile=" + selectedProfile +
                    ", user=" + user +
                    ", error='" + error + '\'' +
                    ", errorMessage='" + errorMessage + '\'' +
                    ", cause='" + cause + '\'' +
                    '}';
        }

    }

    public static class SelectedProfile {

        private String id;
        private String name;

        public String getId() {
            return id;
        }
        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Yggdrasil.SelectedProfile{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }

    }

    public static class User {

        private JsonElement properties;

        public JsonElement getProperties() {
            return properties;
        }

        @Override
        public String toString() {
            return "Yggdrasil.User{" +
                    "properties=" + properties +
                    '}';
        }

    }

    public static class Error {

        protected String error;
        protected String errorMessage;
        protected String cause;

        public String getError() {
            return error;
        }
        public String getErrorMessage() {
            return errorMessage;
        }
        public String getCause() {
            return cause;
        }

        @Override
        public String toString() {
            return "Yggdrasil.Error{" +
                    "error='" + error + '\'' +
                    ", errorMessage='" + errorMessage + '\'' +
                    ", cause='" + cause + '\'' +
                    '}';
        }

    }

}
