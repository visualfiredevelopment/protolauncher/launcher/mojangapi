package net.protolauncher.mojang.library;

import net.protolauncher.mojang.Artifact;

import java.util.HashMap;

/**
 * Represents the primary downloads for a game library such as lwjgl.
 * Can only be created via GSON parsing.
 */
public class LibraryDownloads {

    // Instance Variables
    private Artifact artifact;
    private HashMap<String, Artifact> classifiers;

    // Constructor
    private LibraryDownloads() { }

    // Getters
    public Artifact getArtifact() {
        return artifact;
    }
    public HashMap<String, Artifact> getClassifiers() {
        return classifiers;
    }

    // Stringify
    @Override
    public String toString() {
        return "LibraryDownloads{" +
                "artifact=" + artifact +
                ", classifiers=" + classifiers +
                '}';
    }

}
