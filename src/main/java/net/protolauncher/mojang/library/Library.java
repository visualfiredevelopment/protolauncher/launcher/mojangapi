package net.protolauncher.mojang.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.protolauncher.mojang.Artifact;
import net.protolauncher.mojang.rule.Rule;
import net.protolauncher.util.ISaveable;
import net.protolauncher.util.SystemInfo;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.HashMap;

/**
 * Represents a library.
 * Can only be created via GSON parsing.
 */
public class Library implements ISaveable {

    // Constants
    public static final Path FOLDER_LOCATION; // The file at which these settings are stored
    public static final Gson GSON; // A custom GSON

    // Static Initializer
    static {
        FOLDER_LOCATION = Path.of("mojang/libraries/");
        GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    }

    // Instance Variables
    private LibraryDownloads downloads;
    private HashMap<String, String[]> extract; // A mapping of what needs to be extracted
    private String name;
    private HashMap<String, String> natives; // A mapping of OS details to a native
    private Rule[] rules; // Rules to determine when this library should be included

    // Constructor
    private Library() { }

    // Getters
    public LibraryDownloads getDownloads() {
        return downloads;
    }
    public HashMap<String, String[]> getExtract() {
        return extract;
    }
    public String getName() {
        return name;
    }
    public HashMap<String, String> getNatives() {
        return natives;
    }
    public Rule[] getRules() {
        return rules;
    }


    // Methods
    /**
     * Splits the name into it separate parts to get the details about the library.
     *
     * @return A string array of length 3 with 0 being the path, 1 being the name, and 2 being the version
     */
    public String[] getNameDetails() {
        String[] parts = new String[3];
        int first = name.indexOf(':');
        int second = name.indexOf(':', first + 1);

        parts[0] = name.substring(0, first);
        parts[1] = name.substring(first + 1, second);
        parts[2] = name.substring(second + 1);

        return parts;
    }

    /**
     * Checks all of the natives OS keys against the current {@link SystemInfo#OS_NAME} to see if they should be included.
     *
     * @return The artifact for the targeted natives (there should only be one).
     */
    public Artifact getTargetedNatives() {
        if (downloads == null || downloads.getClassifiers() == null || natives == null) {
            return null;
        }

        // Get the natives for the specified OS, and if it returns null, then return null
        String targetedNatives = natives.get(SystemInfo.OS_NAME);
        if (targetedNatives == null) {
            return null;
        } else {
            String key = targetedNatives.replace("${arch}", SystemInfo.OS_BIT);
            return downloads.getClassifiers().get(key);
        }
    }

    // Implement ISaveable
    @Override
    public Type getType() {
        return TypeToken.get(Library.class).getType();
    }
    @Override
    public Gson getGson() {
        return GSON;
    }
    @Override
    public Path getLocation() {
        String path;
        if (downloads == null || downloads.getArtifact() == null || downloads.getArtifact().getPath() == null) {
            // Construct path manually
            String[] details = this.getNameDetails();
            path = details[0].replace(".", "/") + "/" + details[1] + "/" + details[1] + "-" + details[2] + ".jar";
        } else {
            path = downloads.getArtifact().getPath();
        }
        return FOLDER_LOCATION.resolve(path);
    }

}
