package net.protolauncher.mojang.rule;

import com.google.gson.annotations.SerializedName;

/**
 * An action for a rule, usually ALLOW or DISALLOW
 */
public enum Action {

    @SerializedName("disallow")
    DISALLOW,

    @SerializedName("allow")
    ALLOW

}
