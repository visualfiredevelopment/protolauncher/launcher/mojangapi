package net.protolauncher.mojang.rule;

import net.protolauncher.util.SystemInfo;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a Mojang "rule" for a library or artifact
 */
public class Rule {

    // Instance Variables
    private Action action;
    @Nullable private OperatingSystem os;

    // Constructor
    public Rule(Action action, @Nullable OperatingSystem os) {
        this.action = action;
        this.os = os;
    }
    public Rule(Action action) {
        this.action = action;
        this.os = null;
    }

    // Getters
    public Action getAction() {
        return action;
    }
    public OperatingSystem getOs() {
        return os;
    }

    // Methods
    /**
     * Determines the final rule action from the provided array of rules and whether or not they should be allowed.
     * If any rule fails to pass the arguments, the rule's parent object should not be used.
     *
     * @param rules The rules to use to determine the final action
     * @return The final {@link Action}
     */
    public static Action determineAction(Rule[] rules) {
        // The default action is to disallow
        Action finalAction = Action.DISALLOW;

        // Check rules
        for (Rule rule : rules) {
            // Firstly determine the rule's default action
            Action action = rule.action;

            // Check against OS checks
            if (rule.os != null) {
                // Check the name against the current system name
                if (rule.os.getName() != null && !SystemInfo.OS_NAME.equals(rule.os.getName())) {
                    action = null;
                }

                // Check the version against the current system version
                if (rule.os.getVersion() != null && SystemInfo.OS_VERSION.matches(rule.os.getVersion())) {
                    action = null;
                }

                // Check the architecture against the current system architecture
                if (rule.os.getArch() != null && !SystemInfo.OS_ARCH.equals(rule.os.getArch())) {
                    action = null;
                }
            }

            // If the rule passed all the checks, set the final action to this rule's default action
            if (action != null) {
                finalAction = action;
            }
        }

        // Return the final action after checking all rules
        return finalAction;
    }

    // Stringify
    @Override
    public String toString() {
        return "Rule{" +
                "action=" + action +
                ", os=" + os +
                '}';
    }

}
