package net.protolauncher.mojang.rule;

import org.jetbrains.annotations.Nullable;

/**
 * Represents the 'os' requirement of a rule, but can generically represent any operating system.
 * Can only be created via GSON parsing.
 */
public class OperatingSystem {

    // Instance Variables
    @Nullable private String name;
    @Nullable private String version;
    @Nullable private String arch;

    // Constructor
    private OperatingSystem() { }

    // Getters
    public @Nullable String getName() {
        return name;
    }
    public @Nullable String getVersion() {
        return version;
    }
    public @Nullable String getArch() {
        return arch;
    }

    // Stringify
    @Override
    public String toString() {
        return "System{" +
                "name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", arch='" + arch + '\'' +
                '}';
    }

}
